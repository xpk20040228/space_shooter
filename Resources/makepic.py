from PIL import Image

def rotate_image(image_path, degrees, output_path):
    # Open the image
    img = Image.open(image_path)
    
    # Calculate the size of the expanded canvas
    width, height = img.size
    new_width = int(width * 1.5)
    new_height = int(height * 1.5)
    
    # Create a new image with white background
    new_img = Image.new("RGB", (new_width, new_height), "black")
    
    # Paste the original image into the center of the new image
    new_img.paste(img, ((new_width - width) // 2, (new_height - height) // 2))
    
    # Rotate the image
    rotated_img = new_img.rotate(degrees)
    
    # Save the rotated image
    rotated_img.save(output_path)

# Define the path of the input image
input_image_path = "shpnorm.bmp"

# Rotate the image for 10 degrees each time for all 360 degrees
for i in range(0, 360, 10):
    # Define the path of the output image
    output_image_path = f"shpnorm{36-i//10}.bmp"
    
    # Call the function to rotate the image
    rotate_image(input_image_path, i, output_image_path)

# Print a success message
print("The image was successfully rotated for 10 degrees each time for all 360 degrees.")
