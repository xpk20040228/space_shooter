/*
 * mygame.h: 本檔案儲遊戲本身的class的interface
 * Copyright (C) 2002-2008 Woei-Kae Chen <wkc@csie.ntut.edu.tw>
 *
 * This file is part of game, a free game development framework for windows.
 *
 * game is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * game is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *	 2004-03-02 V4.0
 *      1. Add CGameStateInit, CGameStateRun, and CGameStateOver to
 *         demonstrate the use of states.
 *   2005-09-13
 *      Rewrite the codes for CBall and CEraser.
 *   2005-09-20 V4.2Beta1.
 *   2005-09-29 V4.2Beta2.
 *   2006-02-08 V4.2
 *      1. Rename OnInitialUpdate() -> OnInit().
 *      2. Replace AUDIO_CANYON as AUDIO_NTUT.
 *      3. Add help bitmap to CGameStateRun.
 *   2006-09-09 V4.3
 *      1. Rename Move() and Show() as OnMove and OnShow() to emphasize that they are
 *         event driven.
 *   2008-02-15 V4.4
 *      1. Add namespace game_framework.
 *      2. Replace the demonstration of animation as a new bouncing ball.
 *      3. Use ShowInitProgress(percent) to display loading progress.
*/

namespace game_framework {
	/////////////////////////////////////////////////////////////////////////////
	// Constants
	/////////////////////////////////////////////////////////////////////////////

	enum AUDIO_ID {				// 定義各種音效的編號
		AUDIO_DING,				// 0
		AUDIO_LAKE,				// 1
		AUDIO_NTUT				// 2
	};

	/////////////////////////////////////////////////////////////////////////////
	// 這個class為遊戲的遊戲開頭畫面物件
	// 每個Member function的Implementation都要弄懂
	/////////////////////////////////////////////////////////////////////////////

	class CGameStateInit : public CGameState {
	public:
		CGameStateInit(CGame *g);
		void OnInit();  								// 遊戲的初值及圖形設定
		void OnBeginState();							// 設定每次重玩所需的變數
		void OnKeyUp(UINT, UINT, UINT); 				// 處理鍵盤Up的動作
		void OnLButtonDown(UINT nFlags, CPoint point);  // 處理滑鼠的動作
		void load_backgrounds();
		void high_score_page();
		void main_menu_page();
	protected:
		void OnShow();									// 顯示這個狀態的遊戲畫面
	private:
		CMovingBitmap logo;								// csie的logo
		CMovingBitmap menu_background;
		CMovingBitmap high_score_background;
		int options_main = 1;
		int page = 1;
		string name;
		int highscore = 0;
	};

	/////////////////////////////////////////////////////////////////////////////
	// 這個class為遊戲的遊戲執行物件，主要的遊戲程式都在這裡
	// 每個Member function的Implementation都要弄懂
	/////////////////////////////////////////////////////////////////////////////

	class CGameStateRun : public CGameState {
	public:
		CGameStateRun(CGame *g);
		~CGameStateRun();
		void OnBeginState();							// 設定每次重玩所需的變數
		void OnInit();  								// 遊戲的初值及圖形設定
		void OnKeyDown(UINT, UINT, UINT);
		void OnKeyUp(UINT, UINT, UINT);
		void OnLButtonDown(UINT nFlags, CPoint point);  // 處理滑鼠的動作
		void OnLButtonUp(UINT nFlags, CPoint point);	// 處理滑鼠的動作
		void OnMouseMove(UINT nFlags, CPoint point);	// 處理滑鼠的動作 
		void OnRButtonDown(UINT nFlags, CPoint point);  // 處理滑鼠的動作
		void OnRButtonUp(UINT nFlags, CPoint point);	// 處理滑鼠的動作
		void load_backgrounds();
		void level_page();
		void game_page();
		void pause_page();
		void game_over_page();
		void calculateNewPosition(double& x, double& y, double& character_angle, bool clockwise);
		void reset_game();
		void countdown_convert_str(int seconds, std::string& time_remaining);
		int getRandomNumber(int maxNumber);
		void spawn_npcs(int temp_npc_index);
		void despawn_npcs();
		struct Point {
			double x;
			double y;
		};
		class npc {
		public:
			npc(int shoot_interval) : npc_x(640), npc_y(480), npc_angle(0.0), npc_dist_center(0.0), npc_shoot_interval_current(shoot_interval){
				npc_bitmap.LoadBitmapByString({
					"resources/npcship_easy.bmp",
					"resources/npcship_medium.bmp",
					"resources/npcship_hard.bmp",
					"resources/explode.bmp"
					});
				npc_bitmap.SetTopLeft(640, 480);
				npc_bitmap.SetAnimation(100, true);
			}
			~npc() {}
			int npc_x;
			int npc_y;
			int npc_level;
			int npc_stage = 1;
			int npc_death = 0;
			int npc_shoot_interval_current;
			CMovingBitmap npc_bitmap;
			double npc_angle;
			double npc_dist_center;
		};
		class npc_bullet {
		public:
			int bullet_x, bullet_y, npc_bullet_speed, npc_bullet_damage;
			double bullet_angle;
			CMovingBitmap npc_bullet_bitmap;

			npc_bullet(int x, int y, double angle, int speed, int damage) : bullet_x(x), bullet_y(y), bullet_angle(angle), npc_bullet_speed(speed), npc_bullet_damage(damage){
				npc_bullet_bitmap.LoadBitmapByString({
					"resources/shoot.bmp"
				});
			}
			void move_bullet() {
				bullet_x = static_cast<int>(bullet_x + npc_bullet_speed * cos(bullet_angle));
				bullet_y = static_cast<int>(bullet_y + npc_bullet_speed * sin(bullet_angle));
				npc_bullet_bitmap.SetTopLeft(bullet_x, bullet_y);
			}
		};
		
		
		void character_shoot();
		int bullet_speed;

		class bullet
		{
		public:
			CMovingBitmap bitmap;
			double angle;
		};
		std::vector<bullet> character_bullets;
		//

	protected:
		void OnMove();									// 移動遊戲元素
		void OnShow();									// 顯示這個狀態的遊戲畫面
	private:
		CMovingBitmap game_background;
		CMovingBitmap level_background;
		CMovingBitmap character;
		std::vector<npc> npcs;
		std::vector<npc_bullet> npc_bullets;
		int options_level = 1;
		int options_pause = 1;
		int options_over = 1;
		int recover = 0;
		int page = 1;
		double character_x = 0;
		double character_y = 0;
		double character_angle = 0;
		int character_pic_index = 0;
		int character_status = 0;
		int time_remaining;
		int lives_remaining;
		int pass_time = 0;
		int npc_spawn_time = 10;
		int npc_spawn_time_current = 0;
		int npc_spawn_interval = 50;
		int npc_spawn_num = 3;
		int npc_spawn_status = 0;
		int npc_shoot_interval = 50;
		double npc_speed = 1.0;
		int npc_level;
		int npc_index_current = 1;
		int score = 0;
		int health = 100;
		int highscore = 0;
		string time_remaining_str;
		string name;
	};

	/////////////////////////////////////////////////////////////////////////////
	// 這個class為遊戲的結束狀態(Game Over)
	// 每個Member function的Implementation都要弄懂
	/////////////////////////////////////////////////////////////////////////////

	class CGameStateOver : public CGameState {
	public:
		CGameStateOver(CGame *g);
		void OnBeginState();							// 設定每次重玩所需的變數
		void OnInit();
	protected:
		void OnMove();									// 移動遊戲元素
		void OnShow();									// 顯示這個狀態的遊戲畫面
	private:
		int counter;	// 倒數之計數器
	};
}
