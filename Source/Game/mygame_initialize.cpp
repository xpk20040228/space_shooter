#include "stdafx.h"
#include "../Core/Resource.h"
#include <mmsystem.h>
#include <ddraw.h>
#include "../Library/audio.h"
#include "../Library/gameutil.h"
#include "../Library/gamecore.h"
#include "mygame.h"
#include <fstream>
#include <string>

using namespace game_framework;
/////////////////////////////////////////////////////////////////////////////
// 這個class為遊戲的遊戲開頭畫面物件
/////////////////////////////////////////////////////////////////////////////

CGameStateInit::CGameStateInit(CGame *g) : CGameState(g)
{
}

void CGameStateInit::OnInit()
{
	//
	// 當圖很多時，OnInit載入所有的圖要花很多時間。為避免玩遊戲的人
	//     等的不耐煩，遊戲會出現「Loading ...」，顯示Loading的進度。
	//
	ShowInitProgress(0, "Start Initialize...");	// 一開始的loading進度為0%
	//
	// 開始載入資料
	//
	Sleep(1000);				// 放慢，以便看清楚進度，實際遊戲請刪除此Sleep
	//
	// 此OnInit動作會接到CGameStaterRun::OnInit()，所以進度還沒到100%
	//
	
	load_backgrounds();

}

void CGameStateInit::load_backgrounds()
{
	menu_background.LoadBitmapByString({
		"resources/menu_background.bmp"
		});
	menu_background.SetTopLeft(0, 0);
	high_score_background.LoadBitmapByString({
		"resources/high_score_background.bmp"
		});
	high_score_background.SetTopLeft(0, 0);
}

void CGameStateInit::OnBeginState()
{
	
}

void CGameStateInit::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_RETURN) {
		if (page == 1) {
			if (options_main == 1) {
				GotoGameState(GAME_STATE_RUN);		// 切換至GAME_STATE_RUN
			}
			else if (options_main == 2) {
				high_score_page();
			}
			else if (options_main == 3) {
				exit(0);
			}
		}
		else if (page == 2) {
			main_menu_page();
		}
	}
	else if (nChar == VK_UP) {
		if (options_main == 1) {
			options_main = 3;
		}
		else {
			options_main = options_main - 1;
		}
	}
	else if (nChar == VK_DOWN) {
		if (options_main == 3) {
			options_main = 1;
		}
		else {
			options_main = options_main + 1;
		}
	}
}

void CGameStateInit::OnLButtonDown(UINT nFlags, CPoint point)
{
	
}


void CGameStateInit::OnShow()
{
	if (page == 1)
	{
		menu_background.ShowBitmap();
		CDC* pDC = CDDraw::GetBackCDC();
		if (options_main == 1) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 620, 600, "Play");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 585, 720, "High Score");
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		else if (options_main == 2) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 620, 600, "Play");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 585, 720, "High Score");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		else if (options_main == 3) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 620, 600, "Play");
			CTextDraw::Print(pDC, 585, 720, "High Score");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
	}
	else if (page == 2)
	{
		high_score_background.ShowBitmap();
		CDC* pDC = CDDraw::GetBackCDC();
		CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
		CTextDraw::Print(pDC, 585, 300, "High Score");
		CTextDraw::Print(pDC, 530, 390, "name");
		CTextDraw::Print(pDC, 695, 390, "score");
		CTextDraw::Print(pDC, 530, 500, name);
		CTextDraw::Print(pDC, 695, 500, to_string(highscore));
		CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
		CTextDraw::Print(pDC, 617, 840, "Back");
	}
	CDDraw::ReleaseBackCDC();
}


void CGameStateInit::high_score_page() {
	page = 2;
	ifstream high_score_file("highscore.txt");
	std::getline(high_score_file, name);
	high_score_file >> highscore;
	high_score_file.close();
}

void CGameStateInit::main_menu_page() {
	page = 1;
	options_main = 1;
}

