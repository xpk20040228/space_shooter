#include "stdafx.h"
#include "../Core/Resource.h"
#include <mmsystem.h>
#include <ddraw.h>
#include "../Library/audio.h"
#include "../Library/gameutil.h"
#include "../Library/gamecore.h"
#include "mygame.h"
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <random>
#include <algorithm>
#include <fstream>
#include <string>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include <cmath>
const double DEG_TO_RAD = M_PI / 180.0;
const double RADIUS = 440.0;


using namespace game_framework;

/////////////////////////////////////////////////////////////////////////////
// 這個class為遊戲的遊戲執行物件，主要的遊戲程式都在這裡
/////////////////////////////////////////////////////////////////////////////

CGameStateRun::CGameStateRun(CGame *g) : CGameState(g)
{
}

CGameStateRun::~CGameStateRun()
{
}

void CGameStateRun::OnBeginState()
{
}

void CGameStateRun::OnMove()							// 移動遊戲元素
{
	if (page == 2) {
		if (pass_time >= 50) {
			time_remaining = time_remaining - 1;
			npc_spawn_time_current = npc_spawn_time_current - 1;
			pass_time = 0;
		}
		pass_time = pass_time + 1;
		countdown_convert_str(time_remaining, time_remaining_str);
		
		if (pass_time%npc_spawn_interval==0 && npc_spawn_time_current <=0 && npc_spawn_status==0 && static_cast<int>(npcs.size())<npc_spawn_num ) {
			spawn_npcs(npc_index_current);
			if (static_cast<int>(npcs.size()) == npc_spawn_num) {
				npc_spawn_status = 1;
			}
		}
		if (npc_spawn_time_current < 0 && npcs.size() == 0) {
			npc_spawn_time_current = npc_spawn_time;
			npc_index_current = getRandomNumber(npc_level);
			npc_spawn_status = 0;
		}
		if (time_remaining == 0) {
			game_over_page();
		}
		if (lives_remaining <= 0 && health <= 0) {
			game_over_page();
		}
	}
	else {
		pass_time = 0;
	}
}

void CGameStateRun::OnInit()  								// 遊戲的初值及圖形設定
{
	load_backgrounds();
	ifstream high_score_file("highscore.txt");
	std::getline(high_score_file, name);
	high_score_file >> highscore;
	high_score_file.close();
}

void CGameStateRun::load_backgrounds()
{
	level_background.LoadBitmapByString({
		"resources/menu_background.bmp"
		});
	level_background.SetTopLeft(0, 0);
	game_background.LoadBitmapByString({
		"resources/game_bg_1.bmp",
		"resources/game_bg_2.bmp",
		"resources/game_bg_3.bmp"
		});
	game_background.SetTopLeft(0, 0);
	game_background.SetAnimation(100, false);
	character.LoadBitmapByString({
		"resources/shpnorm0.bmp",
		"resources/shpnorm1.bmp",
		"resources/shpnorm2.bmp",
		"resources/shpnorm3.bmp",
		"resources/shpnorm4.bmp",
		"resources/shpnorm5.bmp",
		"resources/shpnorm6.bmp",
		"resources/shpnorm7.bmp",
		"resources/shpnorm8.bmp",
		"resources/shpnorm9.bmp",
		"resources/shpnorm10.bmp",
		"resources/shpnorm11.bmp",
		"resources/shpnorm12.bmp",
		"resources/shpnorm13.bmp",
		"resources/shpnorm14.bmp",
		"resources/shpnorm15.bmp",
		"resources/shpnorm16.bmp",
		"resources/shpnorm17.bmp",
		"resources/shpnorm18.bmp",
		"resources/shpnorm19.bmp",
		"resources/shpnorm20.bmp",
		"resources/shpnorm21.bmp",
		"resources/shpnorm22.bmp",
		"resources/shpnorm23.bmp",
		"resources/shpnorm24.bmp",
		"resources/shpnorm25.bmp",
		"resources/shpnorm26.bmp",
		"resources/shpnorm27.bmp",
		"resources/shpnorm28.bmp",
		"resources/shpnorm29.bmp",
		"resources/shpnorm30.bmp",
		"resources/shpnorm31.bmp",
		"resources/shpnorm32.bmp",
		"resources/shpnorm33.bmp",
		"resources/shpnorm34.bmp",
		"resources/shpnorm35.bmp",
		"resources/explode.bmp"
		});
	reset_game();
}

void CGameStateRun::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	
}

void CGameStateRun::OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	if (nChar == VK_RETURN)
	{
		if (page == 1) {
			if (options_level == 11)
			{
				GotoGameState(GAME_STATE_INIT);
			}
			else
			{
				game_page();
			}
		}
		if (page == 3) {
			if (options_pause == 1) {
				game_page();
			}
			else if (options_pause == 2) {
				recover = 0;
				page = 1;
				options_pause = 1;
				options_level = 1;
				GotoGameState(GAME_STATE_INIT);
			}
			else if (options_pause == 3) {
				exit(0);
			}
		}
		if (page == 4) {
			if (options_over == 1) {
				recover = 0;
				game_page();
			}
			else if (options_over == 2) {
				recover = 0;
				page = 1;
				options_pause = 1;
				options_level = 1;
				GotoGameState(GAME_STATE_INIT);
			}
			else if (options_over == 3) {
				exit(0);
			}
		}
	}
	else if (nChar == VK_UP) {
		if (page == 1) {
			if (options_level == 1) {
				options_level = 11;
			}
			else {
				options_level = options_level - 1;
			}
		}
		if (page == 3) {
			if (options_pause == 1) {
				options_pause = 3;
			}
			else {
				options_pause = options_pause - 1;
			}
		}
		if (page == 4) {
			if (options_over == 1) {
				options_over = 3;
			}
			else {
				options_over = options_over - 1;
			}
		}
	}
	else if (nChar == VK_DOWN) {
		if (page == 1) {
			if (options_level == 11) {
				options_level = 1;
			}
			else {
				options_level = options_level + 1;
			}
		}
		if (page == 3) {
			if (options_pause == 3) {
				options_pause = 1;
			}
			else {
				options_pause = options_pause + 1;
			}
		}
		if (page == 4) {
			if (options_over == 3) {
				options_over = 1;
			}
			else {
				options_over = options_over + 1;
			}
		}
	}
	else if (nChar == VK_ESCAPE) {
		if (page == 2) {
			pause_page();
		}
	}
	else if (nChar == VK_LEFT) {
		if (page == 2 && character_status>=0) {
			calculateNewPosition(character_x, character_y, character_angle, true);
			character.SetTopLeft(static_cast<int>(round(character_x))+592, static_cast<int>(round(character_y))+440);
			if (character_pic_index == 35) {
				character_pic_index = 0;
			}
			else {
				character_pic_index = character_pic_index + 1;
			}
			character.SetFrameIndexOfBitmap(character_pic_index);
		}
	}
	else if (nChar == VK_RIGHT) {
		if (page == 2 && character_status >= 0) {
			calculateNewPosition(character_x, character_y, character_angle, false);
			character.SetTopLeft(static_cast<int>(round(character_x))+592, static_cast<int>(round(character_y))+440);
			if (character_pic_index == 0) {
				character_pic_index = 35;
			}
			else {
				character_pic_index = character_pic_index - 1;
			}
			character.SetFrameIndexOfBitmap(character_pic_index);
		}
	}
	
	else if (nChar == VK_SPACE)
	{
		if (page == 2 && character_status >= 0)
		{
			character_shoot();
		}
	}
	
}

void CGameStateRun::OnLButtonDown(UINT nFlags, CPoint point)  // 處理滑鼠的動作
{
}

void CGameStateRun::OnLButtonUp(UINT nFlags, CPoint point)	// 處理滑鼠的動作
{
}

void CGameStateRun::OnMouseMove(UINT nFlags, CPoint point)	// 處理滑鼠的動作
{
}

void CGameStateRun::OnRButtonDown(UINT nFlags, CPoint point)  // 處理滑鼠的動作
{
}

void CGameStateRun::OnRButtonUp(UINT nFlags, CPoint point)	// 處理滑鼠的動作
{
}

void CGameStateRun::OnShow()
{
	if (page == 1)
	{
		level_background.ShowBitmap();
		CDC* pDC = CDDraw::GetBackCDC();
		CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
		CTextDraw::Print(pDC, 615, 500, "Level");
		if (options_level == 1) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 2) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 3) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 4) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 5) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 6) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 7) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 8) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 9) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 10) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		else if (options_level == 11) {
			CTextDraw::Print(pDC, 235, 600, "1");
			CTextDraw::Print(pDC, 435, 600, "2");
			CTextDraw::Print(pDC, 635, 600, "3");
			CTextDraw::Print(pDC, 835, 600, "4");
			CTextDraw::Print(pDC, 1035, 600, "5");
			CTextDraw::Print(pDC, 235, 750, "6");
			CTextDraw::Print(pDC, 435, 750, "7");
			CTextDraw::Print(pDC, 635, 750, "8");
			CTextDraw::Print(pDC, 835, 750, "9");
			CTextDraw::Print(pDC, 1035, 750, "10");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 617, 840, "Back");
		}
		CDDraw::ReleaseBackCDC();
	}
	else if (page == 2) {
		game_background.SetAnimation(100, false);
		game_background.ShowBitmap();
		character.ShowBitmap();
		
		if (!npcs.empty()) {
			std::vector<int> del_list_character;
			std::vector<int> del_list_character_bullets;
			for (int i = 0; i < static_cast<int>(npcs.size()); ++i) {
				npc& temp_npc = npcs[i];
				temp_npc.npc_shoot_interval_current = temp_npc.npc_shoot_interval_current - 1;
				if (temp_npc.npc_dist_center <= 230 && temp_npc.npc_stage == 1) {
					temp_npc.npc_x = static_cast<int>(640 + temp_npc.npc_dist_center * cos(temp_npc.npc_angle)); // 640 is the x-coordinate of the center;
					temp_npc.npc_y = static_cast<int>(480 + temp_npc.npc_dist_center * sin(temp_npc.npc_angle)); // 480 is the y-coordinate of the center;

					// Update angle and distance for the next point in the spiral
					temp_npc.npc_angle = temp_npc.npc_angle + 0.04 * npc_speed;
					temp_npc.npc_dist_center = temp_npc.npc_dist_center + 0.25*npc_speed; // Change this value to control the spacing between points in the spiral

					// Here you would add the code to actually move the object on the screen
					// For example:
					temp_npc.npc_bitmap.SetTopLeft(temp_npc.npc_x, temp_npc.npc_y);
					if (temp_npc.npc_dist_center >= 230) {
						temp_npc.npc_stage = 2;
					}
				}
				else if (temp_npc.npc_dist_center >= 0 && temp_npc.npc_stage == 2) {
					temp_npc.npc_x = static_cast<int>(640 + temp_npc.npc_dist_center * cos(temp_npc.npc_angle)); // 640 is the x-coordinate of the center;
					temp_npc.npc_y = static_cast<int>(480 + temp_npc.npc_dist_center * sin(temp_npc.npc_angle)); // 480 is the y-coordinate of the center;

					// Update angle and distance for the next point in the spiral
					temp_npc.npc_angle = temp_npc.npc_angle + 0.04 *-1* npc_speed;
					temp_npc.npc_dist_center = temp_npc.npc_dist_center + 0.25 *-1* npc_speed; // Change this value to control the spacing between points in the spiral

					// Here you would add the code to actually move the object on the screen
					// For example:
					temp_npc.npc_bitmap.SetTopLeft(temp_npc.npc_x, temp_npc.npc_y);
				}
				temp_npc.npc_bitmap.SetTopLeft(temp_npc.npc_x, temp_npc.npc_y);
				temp_npc.npc_bitmap.ShowBitmap();
				if (temp_npc.npc_angle < 0) {
					temp_npc.npc_angle += 2 * M_PI;
				}
				else if (temp_npc.npc_angle > 2 * M_PI) {
					temp_npc.npc_angle -= 2 * M_PI;
				}
				if (temp_npc.npc_stage == 2 && temp_npc.npc_dist_center <= 20) {
					despawn_npcs();
				}
				if (temp_npc.npc_bitmap.GetFrameIndexOfBitmap() == 3) {
					temp_npc.npc_death++;
					if (temp_npc.npc_death >= 30) {
						del_list_character.push_back(i);
					}
				}
				if (abs(temp_npc.npc_angle - character_angle) < 0.017 && temp_npc.npc_shoot_interval_current <=0 && character_status >= 0) { // adjust the threshold as needed
					// Spawn a bullet--
					npc_bullets.push_back(npc_bullet(temp_npc.npc_x, temp_npc.npc_y, character_angle, 18 - bullet_speed, temp_npc.npc_level));
					temp_npc.npc_shoot_interval_current = npc_shoot_interval;
				}
			}
			for (int i = del_list_character.size() - 1; i >= 0; --i) {
				npcs.erase(npcs.begin() + del_list_character[i]);
			}
			for (int i = 0; i < static_cast<int>(character_bullets.size()); ++i) {
				for (int j = 0; j < static_cast<int>(npcs.size()); ++j) {
					if (CMovingBitmap::IsOverlap(npcs[j].npc_bitmap, character_bullets[i].bitmap) && npcs[j].npc_death==0) {
						score = score + 10 * (npcs[j].npc_bitmap.GetFrameIndexOfBitmap() + 1);
						npcs[j].npc_bitmap.SetFrameIndexOfBitmap(3);
						del_list_character_bullets.push_back(i);
					}
				}
			}
			for (int i = del_list_character_bullets.size() - 1; i >= 0; --i) {
				character_bullets.erase(character_bullets.begin() + del_list_character_bullets[i]);
			}
		}
		// Move and display the bullets:
		std::vector<int> del_list_npcs_bullets;
		int index_temp = 0;
		for (auto it = npc_bullets.begin(); it != npc_bullets.end();) {
			it->move_bullet();
			it->npc_bullet_bitmap.ShowBitmap();
			if (CMovingBitmap::IsOverlap(character, it->npc_bullet_bitmap) && character_status >= 0) {
				del_list_npcs_bullets.push_back(index_temp);
				health = health - 25 * (it->npc_bullet_damage);
				character.SetFrameIndexOfBitmap(36);
				if (health <= 0) {
					health = 0;
					character_status = -1;
				}
				else {
					character_status = 1;
				}
			}
			// Check if the bullet is out of screen range
			if (it->bullet_x < 0 || it->bullet_x > 1280 || it->bullet_y < 0 || it->bullet_y > 960) {
				// If the bullet is out of range, remove it from the vector
				it = npc_bullets.erase(it);
			}
			else {
				++it;
				++index_temp;
			}
		}
		for (int i = del_list_npcs_bullets.size() - 1; i >= 0; --i) {
			npc_bullets.erase(npc_bullets.begin() + del_list_npcs_bullets[i]);
		}
		if (character_status > 0) {
			character_status++;
			if (character_status > 10) {
				character_status = 0;
				character.SetFrameIndexOfBitmap(character_pic_index);
			}
		}
		if (character_status < 0 && lives_remaining >0) {
			character_status--;
			if (character_status < -30) {
				character_pic_index = 0;
				character.SetFrameIndexOfBitmap(character_pic_index);
				character.SetTopLeft(592, 880);
				character_status = 0;
				character_x = 0;
				character_y = 0;
				character_angle = 0.5 * M_PI;//Y座標問題，整個都是正的，故不是1.5*PI
				health = 100;
				lives_remaining--;
			}
		}
		CDC* pDC = CDDraw::GetBackCDC();
		CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
		CTextDraw::Print(pDC, 13, 20, "LIVES");
		CTextDraw::Print(pDC, 35, 80, to_string(lives_remaining));

		
		CTextDraw::Print(pDC, 15, 440, "TIME");
		CTextDraw::Print(pDC, 26, 500, time_remaining_str);



		CTextDraw::Print(pDC, 10, 830, "LEVEL");
		CTextDraw::Print(pDC, 35, 890, to_string(options_level));


		CTextDraw::Print(pDC, 1168, 830, "HEALTH");
		CTextDraw::Print(pDC, 1185, 890, to_string(health)+"%");
		


		CTextDraw::Print(pDC, 1175, 20, "SCORE");
		CTextDraw::Print(pDC, 1205, 80, to_string(score));
		CDDraw::ReleaseBackCDC();
		
		for (auto it = character_bullets.begin(); it != character_bullets.end(); )
		{
			int bulletX = it->bitmap.GetLeft();
			int bulletY = it->bitmap.GetTop();
			it->bitmap.SetTopLeft(bulletX - static_cast<int>(bullet_speed*cos(it->angle)), bulletY - static_cast<int>(bullet_speed*sin(it->angle)));
			it->bitmap.ShowBitmap();

			if ((bulletX - 640)*(bulletX - 640) + (bulletY - 480)*(bulletY - 480) < 6400)
			{
				// 如果子彈超出範圍，從 vector 中移除
				it = character_bullets.erase(it);
			}
			else
			{
				++it;
			}
		}
	}
	else if (page == 3) {
		game_background.ToggleAnimation();
		game_background.ShowBitmap();
		CDC* pDC = CDDraw::GetBackCDC();
		CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
		CTextDraw::Print(pDC, 582, 300, "Pause Page");
		if (options_pause == 1) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 620, 600, "Play");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 612, 720, "Menu");
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		else if (options_pause == 2) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 620, 600, "Play");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 612, 720, "Menu");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		else if (options_pause == 3) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 620, 600, "Play");
			CTextDraw::Print(pDC, 612, 720, "Menu");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		CDDraw::ReleaseBackCDC();
	}
	else if (page == 4) {
		game_background.ToggleAnimation();
		game_background.ShowBitmap();
		CDC* pDC = CDDraw::GetBackCDC();
		CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
		CTextDraw::Print(pDC, 582, 200, "Game Over");
		CTextDraw::Print(pDC, 550, 390, "Score:");
		CTextDraw::Print(pDC, 680, 390, to_string(score));
		if (time_remaining == 0) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 590, 250, "You Won!");
		}
		else if (lives_remaining == 0 && health == 0) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 590, 250, "You Died!");
		}
		if (options_over == 1) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 592, 600, "Try Again");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 612, 720, "Menu");
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		else if (options_over == 2) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 592, 600, "Try Again");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 612, 720, "Menu");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		else if (options_over == 3) {
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 255, 255), 800);
			CTextDraw::Print(pDC, 592, 600, "Try Again");
			CTextDraw::Print(pDC, 612, 720, "Menu");
			CTextDraw::ChangeFontLog(pDC, 21, "微軟正黑體", RGB(255, 0, 0), 800);
			CTextDraw::Print(pDC, 622, 840, "Exit");
		}
		CDDraw::ReleaseBackCDC();
	}
}

void CGameStateRun::level_page() {
	page = 1;
	options_level = 1;
	character_bullets.clear();
}

void CGameStateRun::game_page() {
	if (recover == 0) {
		reset_game();
		recover = 1;
	}
	page = 2;
}

void CGameStateRun::pause_page() {
	page = 3;
	options_pause = 1;
}

void CGameStateRun::game_over_page() {
	recover = 0;
	page = 4;
	options_over = 1;
	if (score > highscore) {
		highscore = score;
		std::ofstream file("highscore.txt");
		file << name << std::endl;
		file << highscore << std::endl;
		file.close();
	}
}

void CGameStateRun::calculateNewPosition(double& x, double& y, double& character_angle, bool clockwise) {
	double speed = 10.0; // Move 10 degrees per key press

	// Convert speed to radians
	speed *= DEG_TO_RAD;

	// Update the angle based on the direction of rotation
	character_angle += clockwise ? speed : -speed;
	if (character_angle < 0) {
		character_angle += 2 * M_PI;
	}
	else if (character_angle > 2 * M_PI) {
		character_angle -= 2 * M_PI;
	}

	// Calculate the new x and y coordinates
	x = RADIUS * cos(character_angle);
	y = RADIUS * sin(character_angle);
}

void CGameStateRun::reset_game() {
	character.SetTopLeft(592, 880);
	character.SetFrameIndexOfBitmap(0);
	character_pic_index = 0;
	character_x = 0;
	character_y = 0;
	character_status = 0;
	character_angle = 0.5*M_PI;//Y座標問題，整個都是正的，故不是1.5*PI
	npcs.clear();
	character_bullets.clear();
	npc_bullets.clear();
	npc_spawn_status = 0;
	if (options_level == 1) {
		time_remaining = 90;
		lives_remaining =5;
		health = 100;
		npc_level = 1;
		npc_spawn_time = 6;
		npc_spawn_num = 3;
		npc_spawn_interval = 25;
		npc_speed = 1;
		npc_shoot_interval = 50;
		bullet_speed = 12;

	}
	else if (options_level == 2) {
		time_remaining = 120;
		lives_remaining = 5;
		health = 100;
		npc_level = 1;
		npc_spawn_time = 6;
		npc_spawn_num = 3;
		npc_spawn_interval = 25;
		npc_speed = 1.2;
		npc_shoot_interval = 50;
		bullet_speed = 12;
		
	}
	else if (options_level == 3) {
		time_remaining = 150;
		lives_remaining = 3;
		health = 100;
		npc_level = 1;
		npc_spawn_time = 5;
		npc_spawn_num = 3;
		npc_spawn_interval = 12;
		npc_speed = 1;
		npc_shoot_interval = 50;
		bullet_speed = 12;
		
	}
	else if (options_level == 4) {
		time_remaining = 180;
		lives_remaining = 4;
		health = 100;
		npc_level = 2;
		npc_spawn_time = 4;
		npc_spawn_num = 4;
		npc_spawn_interval = 16;
		npc_speed = 1.5;
		npc_shoot_interval = 50;
		bullet_speed = 11;
	}
	else if (options_level == 5) {
		time_remaining = 240;
		lives_remaining = 3;
		health = 100;
		npc_level = 2;
		npc_spawn_time = 4;
		npc_spawn_num = 4;
		npc_spawn_interval = 16;
		npc_speed = 1.5;
		npc_shoot_interval = 50;
		bullet_speed = 10;
	}
	else if (options_level == 6) {
		time_remaining = 300;
		lives_remaining = 3;
		health = 100;
		npc_level = 2;
		npc_spawn_time = 4;
		npc_spawn_num = 4;
		npc_spawn_interval = 12;
		npc_speed = 1.75;
		npc_shoot_interval = 50;
		bullet_speed = 9;

	}
	else if (options_level == 7) {
		time_remaining = 360;
		lives_remaining = 2;
		health = 100;
		npc_level = 3;
		npc_spawn_time = 3;
		npc_spawn_num = 5;
		npc_spawn_interval = 12;
		npc_speed = 1.75;
		npc_shoot_interval = 50;
		
		bullet_speed = 8;
		//
	}
	else if (options_level == 8) {
		time_remaining = 360;
		lives_remaining = 1;
		health = 100;
		npc_level =3;
		npc_spawn_time = 3;
		npc_spawn_num = 5;
		npc_spawn_interval = 12;
		npc_speed = 2.0;
		npc_shoot_interval = 50;
		
		bullet_speed = 7;
		//
	}
	else if (options_level == 9) {
		time_remaining = 480;
		lives_remaining = 0;
		health = 100;
		npc_level = 3;
		npc_spawn_time = 2;
		npc_spawn_num = 6;
		npc_spawn_interval = 10;
		npc_speed = 2.5;
		npc_shoot_interval = 50;
		
		bullet_speed = 6;
		//
	}
	else if (options_level == 10) {
		time_remaining = 1800;
		lives_remaining = 10;
		health = 1000;
		npc_level = 3;
		npc_spawn_time = 2;
		npc_spawn_num = 6;
		npc_spawn_interval = 10;
		npc_speed = 2.5;
		npc_shoot_interval = 20;
		
		bullet_speed = 6;
		//
	}
	npc_index_current = 1;
	npc_spawn_time_current = npc_spawn_time;
	pass_time = 0;
	score = 0;
}

void CGameStateRun::countdown_convert_str(int seconds, std::string& time_remaining) {
	std::ostringstream oss;
	oss << std::to_string(seconds / 60) << ":" << std::setw(2) << std::setfill('0') << std::to_string(seconds % 60);
	time_remaining = oss.str();
}

int CGameStateRun::getRandomNumber(int maxNumber) {
	if (maxNumber == 1) {
		return 1; // If the number is 1, keep it as 1
	}
	// Create a random number generator
	std::random_device rd;  // Obtain a random number from hardware
	std::mt19937 gen(rd()); // Seed the generator
	std::uniform_int_distribution<> distr(1, maxNumber); // Define the range

	return distr(gen); // Generate a random number between 1 and maxNumber
}

void CGameStateRun::spawn_npcs(int temp_npc_index) {
	npc temp_npc = npc(npc_shoot_interval);
	temp_npc.npc_bitmap.SetFrameIndexOfBitmap(temp_npc_index-1);
	temp_npc.npc_level = temp_npc_index;
	npcs.insert(npcs.begin(), temp_npc);
}

void CGameStateRun::despawn_npcs() {
	npcs.pop_back();
}



void CGameStateRun::character_shoot()
{
	bullet new_bullet;
	new_bullet.bitmap.LoadBitmapByString({ "resources/shoot.bmp" });
	int x = character.GetLeft() + 45 + static_cast<int>(45 * cos(character_angle + M_PI));
	int y = character.GetTop() + 20 - static_cast<int>(50 * sin(character_angle));
	new_bullet.bitmap.SetTopLeft(x, y);
	new_bullet.angle = character_angle;  // 儲存當前的角度
	character_bullets.push_back(new_bullet);

}
//
